function Register() {
    // 檢查 email
    var email = $('#email').val()

    if (!email) {
        $('#spHint').text('請填入 email')
        return
    }

    var name = $('#name').val()
    var password = $('#password').val()
    var password2 = $('#password2').val()    

    var postData = {
        email: email,
        name: name,
        password: password,
    }

    AjaxPost('/api/register', postData, cbRegister, cbRegisterErr)
}

function cbRegister(ret) {
    $('#spHint').text(ret.message)
}

function cbRegisterErr(ret) {
    err = ret.responseJSON
    $('#spHint').text(err.message)
}