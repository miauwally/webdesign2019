from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from django.core.files.storage import FileSystemStorage

import json
import os

import misc.httpHelper as httpHelper

import sql.sqlAccounts as sqlAccounts

@csrf_exempt
def register(request):
    email = httpHelper.getValue(request, 'email')
    name = httpHelper.getValue(request, 'name')
    password = httpHelper.getValue(request, 'password')

    account = sqlAccounts.selectByEmail(email)

    if account:
        return httpHelper.rspError('EMAIL_EXISTED')

    sqlAccounts.add(email, name, password)

    return httpHelper.rspJson('OK')